<?php
/**
 * @file
 * Contains the standard page callbacks for the silverpop module.
 */

/**
 * Returns Silverpop lists.
 *
 * @return string
 *   HTML table of lists.
 */
 function silverpop_getlists() {
  if (variable_get('silverpop_api_engage', FALSE)){
    $silverpop = new Silverpop();
    return $silverpop->getlists();
  }
  else {
    drupal_set_message(t('Silverpop settings are not configured. Please configure Silverpop settings.'), 'error');
    drupal_goto('admin/config/services/silverpop');
  }
 }

/**
 * Menu callback: Send tab page.
 *
 * @param object $node
 *   A node object.
 */
function silverpop_node_tab_page($node) {
  drupal_set_title(t('Send mailing'));
  return drupal_get_form('silverpop_node_tab_schedule_form', $node);
}

/**
 * Form constructor for schedule form.
 *
 * @param object $node
 *   A node object.
 *
 * @see silverpop_node_tab_schedule_form_validate()
 * @see silverpop_node_tab_schedule_form_submit()
 */
function silverpop_node_tab_schedule_form($form, &$form_state, $node) {
  $address_default = variable_get('site_mail', ini_get('sendmail_from'));
  $form['mail_name'] = array(
    '#title' => t('Mailing name'),
    '#type' => 'textfield',
    '#maxlength' => 40,
    '#size' => 40,
    '#description' => t('Must be a unique name (not match with an exhisting mailing name in silverpop)'),
    '#required' => TRUE,
  );
  $form['from_name'] = array(
    '#title' => t('From name'),
    '#type' => 'textfield',
    '#maxlength' => 40,
    '#size' => 40,
    '#required' => TRUE,
  );
  $form['from_address'] = array(
    '#title' => t('From address (email)'),
    '#type' => 'textfield',
    '#maxlength' => 40,
    '#size' => 40,
    '#required' => TRUE,
  );
  $form['reply_to'] = array(
    '#title' => t('Reply to (email)'),
    '#type' => 'textfield',
    '#maxlength' => 40,
    '#size' => 40,
    '#required' => TRUE,
  );
  $form['send_type'] = array(
    '#type' => 'radios',
    '#options' => array(
      'now' => t('Now'),
      'schedule' => t('Schedule'),
    // 'test' => t('To test email address'),
    ),
    '#default_value' => 'now',
    '#title' => t('How will you mail?'),
  );
  $form['scheduled'] = array(
    '#type' => 'date_popup',
    '#title' => t('Schedule time'),
    '#date_format' => 'm/d/Y - H:i:s',
    '#date_year_range' => '-1:+2',
    '#states' => array(
      'visible' => array(
        ':input[name="send_type"]' => array('value' => 'schedule'),
      ),
    ),
  );
  /*
  $form['test_email'] = array(
    '#title' => t('Test email'),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 60,
    '#default_value' => variable_get('silverpop_test_address', $address_default),
    '#states' => array(
      'visible' => array(
        ':input[name="send_type"]' => array('value' => 'test'),
      ),
    ),
  );
  */
  $form['template_id'] = array(
    '#type' => 'value',
    '#value' => variable_get('silverpop_template_' . $node->type, ''),
  );
  $form['placeholder'] = array(
    '#type' => 'value',
    '#value' => variable_get('silverpop_placeholder_' . $node->type, ''),
  );
  $form['list_id'] = array(
    '#type' => 'value',
    '#value' => variable_get('silverpop_list_' . $node->type, ''),
  );
  $form['title'] = array(
    '#type' => 'value',
    '#value' => $node->title,
  );

  // Takes a template for node type. If content type "news" is selected - it
  // will take node--news.tpl.php (if it's present), or core node.tpl.php if
  // not.
  $themed = theme('node', node_view($node));

  $form['body'] = array(
    '#type' => 'value',
    '#value' => silverpop_prepare_text($themed),  /* prepare text (replace  strings like '&nbsp;', because we discovered if we include html with & it causes an error fron silverpop)*/
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_node_tab_schedule_form().
 *
 * @see silverpop_node_tab_schedule_form()
 * @see silverpop_node_tab_schedule_form_submit()
 */
function silverpop_node_tab_schedule_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['from_address'])) {
    form_set_error('from_address', t('This email: @email is not valid!', array('@email' => $form_state['values']['from_address'])));  /* check valid email adress ''From */
  }
  if (!valid_email_address($form_state['values']['reply_to'])) {
    form_set_error('reply_to', t('This email: @email is not valid!', array('@email' => $form_state['values']['reply_to'])));  /* check valid email adress 'Reply to'*/
  }
  if ($form_state['values']['send_type'] == 'schedule') {
    $schedule = strtotime($form_state['values']['scheduled']);
    if ($schedule <= time()) {
      form_set_error('scheduled', t('Scheduled time must be greater'));
    }
  }
}

/**
 * Form submission handler for silverpop_node_tab_schedule_form().
 *
 * @see silverpop_node_tab_schedule_form()
 * @see silverpop_node_tab_schedule_form_validate()
 */
function silverpop_node_tab_schedule_form_submit(&$form, &$form_state) {
  $tid = $form_state['values']['template_id'];
  $list = $form_state['values']['list_id'];
  $subject = $form_state['values']['title'];
  $mailname = $form_state['values']['mail_name'];
  $fromname = $form_state['values']['from_name'];
  $fromaddress = $form_state['values']['from_address'];
  $replyto = $form_state['values']['reply_to'];
  $htmlbody = $form_state['values']['body'];
  $placeholder = $form_state['values']['placeholder'];

  $silverpop = new Silverpop();
  if ($form_state['values']['send_type'] == 'now') {
    $schedule = format_date(time(), 'custom', "m/d/Y  g:i:s A", $timezone = NULL, $langcode = NULL);  /* format date for send now*/
    $silverpop->schedulemailing($tid, $list, $mailname, $subject, $fromname, $fromaddress, $replyto, $schedule, $placeholder, $htmlbody);
  }
  elseif ($form_state['values']['send_type'] == 'schedule') {
    $schedule = format_date(strtotime($form_state['values']['scheduled']), 'custom', "m/d/Y  g:i:s A", $timezone = NULL, $langcode = NULL); /* format date for send schedule*/
    $silverpop->schedulemailing($tid, $list, $mailname, $subject, $fromname, $fromaddress, $replyto, $schedule, $placeholder, $htmlbody);
  }
}
