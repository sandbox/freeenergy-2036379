================
Silverpop
================

This module provides integration with the Silverpop email marketing services.
silverpop.module provides basic configuration and API integration.

TO USE THIS MODULE YOU MUST HAVE AN ACCOUNT WITH SILVERPOP.COM

## Features
  * Choose from different content types as a template for HTML email mailing
    in Silverpop,send node as email.
  * API integration.
  * Add mass contacts to Silverpopop.
  * Send mailing immediately or at a scheduled time.


## API calls used in UI
	CreateTable - create table in Silverpop database.
	GetLists- view Silverpop Contact Lists.
	CreateContactList - Create contact list.
	AddRecipient - Adds email contact to database.
	RemoveRecipient - Remove email contact from database.
	UpdateRecipient - Update recipient data.
	AddContactToContactList - Add email contact to contact list.
	GetListMetaData - View contact list meta data.

  
## Installation Notes
  * Place the entirety of this directory in sites/all/modules/silverpop.
  * Navigate to administer >> modules. Enable silverpop module.


## Configuration
  1. Point your browser to http://example.com/admin/config/silverpop.
  2. Enter your Username and password you use to login to your silverpop
     account.
  3. Select your host (application name) that you will use, (described below).
     If your organization accesses Engage data through the
	 engage1.silverpop.com URL, you should use the XML APIs on Pod 1
	 (api1.silverpop.com/XMLAPI). If you access Engage through the
	 engage2.silverpop.com URL, then you should use  the XML APIs on
	 Pod 2, if you do not know this information contact your Silverpop
	 representative.
  4. Enter the Database ID from silverpop that you will be use, this will be the 
     database the module uses, the module will not effect any other databases. 
   
   
   
 