<?php
/**
* @file
* Implementations of Silverpop XML functions.
*/

//require_once(drupal_get_path('module', 'ari_connect') . '/lib/nusoap.php');

class Silverpop {

  /**
   * The Silverpop session ID.
   *
   * @var string
   */
  protected $jsessionid = '';

  /**
   * The Silverpop API URL.
   *
   * @var string
   */
  protected $api_url = '';

  /**
   * Constructs a Silverpop object.
   */
  public function __construct() {
    $this->api_url = 'http://' . variable_get('silverpop_api_engage', FALSE) . '/XMLAPI';
    $this->login();
  }

  /**
   * Destroys a Silverpop object.
   */
  public function __destruct() {
    $this->logout();
  }

  /**
   * Post data to the Silverpop remote API.
   *
   * @param string $data
   *   The data to post to Silverpop.
   *
   * @return SimpleXMLElement
   *   The response from the Silverpop API in XML format.
   */
  protected function executeCall($data) {
    $options = array(
      'method' => 'POST',
      'data' => 'xml=' . $data->asXML(),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $result = drupal_http_request($this->api_url, $options);

    return simplexml_load_string($result->data);
  }

  /**
   * Silverpop login integration.
   */
  protected function login() {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $login = $body->addChild('Login');
    $login->addChild('USERNAME', variable_get('silverpop_api_username', ''));
    $login->addChild('PASSWORD', variable_get('silverpop_api_password', ''));

    $xml = $this->executeCall($data);

    $jsessionid = (string) $xml->Body->RESULT->SESSIONID;

    if ($jsessionid) {
      $this->jsessionid = $jsessionid;
    }
    else {
      throw new Exception('Login failed.');
    }
  }

  /**
   * Silverpop schedule mailing integration.
   */
  function schedulemailing($tid, $listid, $mailname, $subject, $fromname, $fromaddress, $replyto, $schedule, $placeholder, $htmlbody) {
    // Values for mailing including html in the email body.
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $mailing = $body->addChild('ScheduleMailing');
    $mailing->addChild('TEMPLATE_ID', $tid);
    $mailing->addChild('LIST_ID', $listid);
    $mailing->addChild('MAILING_NAME', $mailname);
    $mailing->addChild('SEND_HTML');
    $mailing->addChild('SEND_AOL');
    $mailing->addChild('SEND_TEXT');
    $mailing->addChild('SUBJECT', $subject);
    $mailing->addChild('FROM_NAME', $fromname);
    $mailing->addChild('FROM_ADDRESS', $fromaddress);
    $mailing->addChild('REPLY_TO', $replyto);
    $mailing->addChild('VISIBILITY', 0);
    $mailing->addChild('SCHEDULED', $schedule);

    $substitutions = $mailing->addChild('SUBSTITUTIONS');
    $substitution = $substitutions->addChild('SUBSTITUTION');
    $substitution->addChild('NAME', $placeholder);
    // Include the node HTML into the body of the mailing email.
    $substitution->addChild('VALUE', '<![CDATA[' . $htmlbody . ']]>');

    drupal_set_message('Recipient OptedIn:<pre>'. check_plain(print_r($data, 1)) . '</pre>');
    $xml = $this->executeCall($data);
    drupal_set_message('debug :<pre>'. check_plain(print_r($xml->Body, 1)) . '</pre>');

    return (string) $xml->Body->RESULT;
  }

  /**
   * Silverpop create table integration.
   */
  function createtable($table_user, $table_name = 'My list') {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $table = $body->addChild('CreateTable');
    $table->addChild('TABLE_NAME', $table_user);

    $columns = $table->addChild('COLUMNS');

    $name = $columns->addChild('COLUMN');
    $name->addChild('NAME', $table_name);
    $name->addChild('TYPE', 'NUMERIC');
    $name->addChild('IS_REQUIRED', 'true');
    $name->addChild('KEY_COLUMN', 'true');

    $date = $columns->addChild('COLUMN');
    $date->addChild('NAME', 'Purchase Date');
    $date->addChild('TYPE', 'DATE');
    $date->addChild('IS_REQUIRED', 'true');

    $id = $columns->addChild('COLUMN');
    $date->addChild('NAME', 'Purchase Id');
    $date->addChild('TYPE', 'NUMERIC');
    $date->addChild('IS_REQUIRED', 'true');

    $xml = $this->executeCall($data);

    if($xml->Body->RESULT->SUCCESS == 'TRUE') {
      $tabid = (string) $xml->Body->RESULT->TABLE_ID;
      return $tabid;
    }
    else {
      return  FALSE;
    }
  }

  /**
   * Silverpop get lists integration.
   */
  function getlists() {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $recipient = $body->addChild('GetLists');
    $recipient->addChild('VISIBILITY', 0);
    $recipient->addChild('LIST_TYPE', 2);

    $xml = $this->executeCall($data);

    $items = (array) $xml->Body->RESULT;
    $row = array();
    $rows = array();
    foreach($items['LIST'] as $item => $v) {
      $row['id'] = (string) $v->ID;
      $row['name'] = (string) $v->NAME;
      $row['parent'] = (string) $v->PARENT_NAME;
      $rows[] = $row;
    }
    
    return $rows;
  }

  /**
   * Silverpop create contact list integration.
   */
  function create_contactlist($db_id, $list_name = 'My list') {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $recipient = $body->addChild('CreateContactList');
    $recipient->addChild('DATABASE_ID', $db_id);
    $recipient->addChild('CONTACT_LIST_NAME', $list_name);
    $recipient->addChild('VISIBILITY', 0);
    $recipient->addChild('PARENT_FOLDER_PATH');

    $xml = $this->executeCall($data);

    drupal_set_message('debug silverpop:<pre>'. check_plain(print_r($xml->Body->RESULT, 1)) . '</pre>');
    $listid = (string) $xml->Body->RESULT->CONTACT_LIST_ID;

    return  $listid ? $listid : FALSE;
  }

  /**
   * Silverpop add recipient integration.
   */
  function addrecipient($listid, $mail) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $recipient = $body->addChild('AddRecipient');
    $recipient->addChild('LIST_ID', $listid);
    $recipient->addChild('CREATED_FROM', 2);
    $recipient->addChild('UPDATE_IF_FOUND', 'true');

    $email = $recipient->addChild('COLUMN');
    $email->addChild('NAME', 'EMAIL');
    $email->addChild('VALUE', $mail);

    $xml = $this->executeCall($data);

    drupal_set_message('debug recipient id is:<pre>' . check_plain(print_r($xml->Body->RESULT, 1)) . '</pre>');
    $recipientid = (string) $xml->Body->RESULT->RecipientId;

    return  $recipientid ? $recipientid : FALSE;
  }

  /**
   * Silverpop remove recipient integration.
   */
  function removerecipient($listid, $mail) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $recipient = $body->addChild('RemoveRecipient');
    $recipient->addChild('LIST_ID', $listid);
    $recipient->addChild('EMAIL', $mail);

    $xml = $this->executeCall($data);

    return (string) $xml->Body->RESULT->SUCCESS;
  }

  /**
   * Silverpop select recipient integration.
   */
  function selectrepicientdata($listid, $mail) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $recipientdata = $body->addChild('SelectRecipientData');
    $recipientdata->addChild('LIST_ID', $listid);
    $recipientdata->addChild('EMAIL', $mail);

    $column = $recipientdata->addChild('COLUMN');
    $column->addChild('NAME', 'Customer Id');

    $xml = $this->executeCall($data);

    if($xml->Body->RESULT->SUCCESS == 'TRUE') {
      $recipient = array();
      $recipient['mail'] = (string) $xml->Body->RESULT->EMAIL;
      $recipient['id'] = (string) $xml->Body->RESULT->RecipientId;
      $recipient['LastModified'] = (string) $xml->Body->RESULT->LastModified;
      $recipient['OptedIn'] = (string) $xml->Body->RESULT->OptedIn;
      return $recipient;
    }
    else {
        return FALSE;
    }
  }

  /**
   * Silverpop update recipient integration.
   */
  function updaterecipient($listid, $oldmail, $newmail) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $recipientdata = $body->addChild('UpdateRecipient');
    $recipientdata->addChild('LIST_ID', $listid);
    $recipientdata->addChild('CREATED_FROM', 2);
    $recipientdata->addChild('OLD_EMAIL', $oldmail);

    $email = $recipientdata->addChild('COLUMN');
    $email->addChild('NAME', 'EMAIL');
    $email->addChild('VALUE', $newemail);

    $lname = $recipientdata->addChild('COLUMN');
    $lname->addChild('NAME', 'Lname');
    $lname->addChild('VALUE', 'Special');

    $xml = $this->executeCall($data);

    return (string) $xml->Body->RESULT->SUCCESS;
  }

  /**
   * Silverpop add contact to contact list integration.
   */
  function addcontacttocontactlist($listid, $contactid) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $contact = $body->addChild('AddContactToContactList');
    $contact->addChild('CONTACT_LIST_ID', $listid);
    $contact->addChild('CONTACT_ID', $contactid);

    $xml = $this->executeCall($data);

    return (string) $xml->Body->RESULT->SUCCESS;
  }

  /**
   * Silverpop get list metadata integration.
   */
  function getlistmetadata($listid) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $listmetadata = $body->addChild('GetListMetaData');
    $listmetadata->addChild('LIST_ID', $listid);

    $xml = $this->executeCall($data);

    return $xml->Body->RESULT;
  }

  /**
   * Silverpop preview mailing integration.
   */
  function previewmailing($mid) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $mailing = $body->addChild('PreviewMailing');
    $mailing->addChild('MailingId', $mid);

    $xml = $this->executeCall($data);

    return (string) $xml->Body->RESULT->HTMLBody;
  }

  /**
   * Silverpop get mailings templates integration.
   */
  function getmailingstemplates($date_start, $date_end) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $mailing = $body->addChild('GetMailingTemplates');
    $mailing->addChild('VISIBILITY', 0);
    $mailing->addChild('LAST_MODIFIED_START_DATE', $date_start);
    $mailing->addChild('LAST_MODIFIED_END_DATE', $date_end);

    $xml = $this->executeCall($data);

    $data = array();
    $data['mid'] = (string) $xml->Body->RESULT->MAILING_TEMPLATE->MAILING_ID;
    $data['msubject'] = (string) $xml->Body->RESULT->MAILING_TEMPLATE->SUBJECT;
    $data['lastmod'] = (string) $xml->Body->RESULT->MAILING_TEMPLATE->LAST_MODIFIED;
    drupal_set_message('debug your table id is:<pre>'. check_plain(print_r($data, 1)) . '</pre>');

    return $data;
  }

  /**
   * Silverpop get sent mailings for user integration.
   */
  function getsentmailingsforuser($date_start, $date_end) {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $mailing = $body->addChild('GetSentMailingsForUser');
    $mailing->addChild('DATE_START', $date_start);
    $mailing->addChild('DATE_END', $date_end);

    $xml = $this->executeCall($data);

    return (string) $xml->Body->RESULT;
  }

  /**
   * Implements logout from Silverpop.
   */
  function logout() {
    $data = new SimpleXMLElement('<Envelope></Envelope>');
    $body = $data->addChild('Body');

    $mailing = $body->addChild('Logout');

    $this->executeCall($data);
  }
}
