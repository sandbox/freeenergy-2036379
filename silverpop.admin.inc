<?php
/**
 * @file
 * Contains the admin page callbacks for the silverpop module.
 */

/**
 * Form constructor for Silverpop settings.
 *
 * @see silverpop_settings_form_validate()
 */
function silverpop_settings_form($form, &$form_state) {
  $address_default = variable_get('site_mail', ini_get('sendmail_from'));
  $nodes = node_type_get_types();
  $types = array();
  foreach ($nodes as $node) {
    $types[$node->type] = $node->name;  /* create array with all content types*/
  }
  $form = array();
  /* implements config form*/
  $form['admin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Silverpop Mail Admin Options'),
    '#collapsible' => FALSE,
    '#weight' => -99,
  );

  $admin_options = array(
    'create-list' => 'Create contact list',
    'add-recipients' => 'Add recipients',
    'remove-recipient' => 'Remove recipient',
    'get-lists' => 'Get Lists',
    'recipient-data' => 'Search recipient data',
    'update-recipient' => 'Update recipient data',
    'add-table' => 'Add table',
    'get-list-meta-data' => 'Get list metadata',
    'add-contact-to-contact-list' => 'Add contact to contact list',
    'get-sent-mailings-for-user' => 'Get sent mailings for user',
    'get-mailings-templates' => 'Get mailings templates',
    'recipients' => 'Recipients',
  );

  $admin_options_list = '<ul>';

  foreach ($admin_options as $val => $title){
    $admin_options_list .= '<li>'.l($title, 'admin/config/services/silverpop/'.$val).'</li>';
  }

  $admin_options_list .= '</ul>';

  $form['admin']['options'] = array(
    '#markup' => $admin_options_list,
  );

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Silverpop API settings'),
    '#description' => t('Use your Silverpop.com login information for these username and password fields.'),
    '#collapsible' => FALSE,
    '#weight' => -10,
  );
  $form['api']['silverpop_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Should be in the form of an e-mail address.'),
    '#default_value' => variable_get('silverpop_api_username', ''),
    '#required' => TRUE,
  );
  $form['api']['silverpop_api_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Enter the password used when logging into Silverpop.'),
    '#default_value' => variable_get('silverpop_api_password', ''),
    '#required' => TRUE,
  );
  $options = array(
    'api0.silverpop.com' => 'Engage Pod 0',
    'api1.silverpop.com' => 'Engage Pod 1',
    'api2.silverpop.com' => 'Engage Pod 2',
    'api3.silverpop.com' => 'Engage Pod 3',
    'api4.silverpop.com' => 'Engage Pod 4',
    'api5.silverpop.com' => 'Engage Pod 5',
  );
  $form['api']['silverpop_api_engage'] = array(
    '#type' => 'select',
    '#title' => t('Host URL (provided to you in documentation from your Silverpop representative'),
    '#description' => t('Your application name. Provided to you in your Silverpop API documentation'),
    '#options' => $options,
    '#default_value' => variable_get('silverpop_api_engage', FALSE),
  );
  $form['api']['silverpop_api_baseid'] = array(
    '#type' => 'textfield',
    '#title' => t('Database ID'),
    '#description' => t('The Database ID, used to create your working database for this module.'),
    '#default_value' => variable_get('silverpop_api_baseid', FALSE),
    '#size' => 20,
    '#maxlength' => 20,
    '#required' => TRUE,
  );
  $form['api']['silverpop_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types for mailings'),
    '#options' => $types,
    '#default_value' => variable_get('silverpop_types', array()),
    '#description' => t('Choose what content type you will be use for sending mail'),
  );
  return system_settings_form($form);
}

/**
 * Form validation handler for silverpop_settings_form().
 *
 * @see silverpop_settings_form()
 */
function silverpop_settings_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['silverpop_api_baseid'])) {
    form_set_error('silverpop_api_baseid', t('db id not valid'));
  }
}

/**
 * Form constructor for the add recipients form.
 */
function silverpop_addrecipient_form() {
  $form['listid'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter list id'),
    '#required' => TRUE,
  );
  $form['email'] = array(
    '#title' => t('Enter emails one per line'),
    '#type' => 'textarea',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_addrecipient_form().
 *
 * Check valid email.
 *
 * @see silverpop_addrecipient_form()
 * @see silverpop_addrecipient_form_submit()
 */
function silverpop_addrecipient_form_validate($form, &$form_state) {
  foreach (explode("\n", str_replace("\r", '', $form_state['values']['email'])) as $email) {
    if (!valid_email_address($email)) {
      form_set_error('email', t('This email: @email is not valid!', array('@email' => $email)));
    }
  }
}

/**
 * Form submission handler for silverpop_addrecipient_form().
 *
 * Add recipient.
 *
 * @see silverpop_addrecipient_form()
 * @see silverpop_addrecipient_form_validate()
 */
function silverpop_addrecipient_form_submit($form, &$form_state) {
  if ($form_state['values']['email']) {
    $silverpop = new Silverpop();
    foreach (explode("\n", str_replace("\r", '', $form_state['values']['email'])) as $email) {
      $recipientid = $silverpop->addrecipient($form_state['values']['listid'], $email);
      drupal_set_message(t('Recipient id is: %recipient', array('%recipient' => print_r($recipientid, 1))));
    }
  }
}

/**
 * Form constructor for the remove recipient form.
 *
 * @see silverpop_removerecipient_form_submit()
 */
function silverpop_removerecipient_form() {
  $form['delemail'] = array(
    '#title' => t('Enter email'),
    '#type' => 'textfield',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  return $form;
}

/**
 * Form submission handler for silverpop_removerecipient_form().
 *
 * @see silverpop_removerecipient_form()
 */
function silverpop_removerecipient_form_submit($form, &$form_state) {
  if ($form_state['values']['delemail']) {
    $silverpop = new Silverpop();
    $result = $silverpop->removerecipient(variable_get('silverpop_api_baseid', FALSE), trim($form_state['values']['delemail']));
    if ($result == 'TRUE') {
      drupal_set_message(t('Recipient removed!'));
    }
    elseif ($result == 'false') {
      drupal_set_message(t('Error! Recipient is not a member of this list!'));
    }
  }
}

/**
 * Form constructor for get recipient data.
 *
 * @see silverpop_recipientdata_form_validate()
 * @see silverpop_recipientdata_form_submit()
 */
function silverpop_recipientdata_form() {
  $form['email'] = array(
    '#title' => t('Enter recipient email'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_recipientdata_form().
 *
 * Check valid email.
 *
 * @see silverpop_recipientdata_form()
 * @see silverpop_recipientdata_form_submit()
 */
function silverpop_recipientdata_form_validate($form, &$form_state) {
  foreach (explode("\n", str_replace("\r", '', $form_state['values']['email'])) as $email) {
    if (!valid_email_address($email)) {
      form_set_error('email', t('This email: @email is not valid!', array('@email' => $email)));
    }
  }
}

/**
 * Form submission handler for silverpop_recipientdata_form().
 *
 * Get recipient data.
 *
 * @see silverpop_recipientdata_form()
 * @see silverpop_recipientdata_form_validate()
 */
function silverpop_recipientdata_form_submit($form, &$form_state) {
  $silverpop = new Silverpop();
  $result = $silverpop->selectrepicientdata(variable_get('silverpop_api_baseid', FALSE), $form_state['values']['email']);
  if ($result) {
    drupal_set_message(t('Recipient email is: %recipient', array(
      '%recipient' => print_r($result['mail'], 1),
    )));
    drupal_set_message(t('Recipient id is: %id', array(
      '%id' => print_r($result['id'], 1),
    )));
    drupal_set_message(t('Recipient Last Modified: %modified', array(
      '%modified' => print_r($result['LastModified'], 1),
    )));
    drupal_set_message(t('Recipient OptedIn: %optedin', array(
      '%optedin' => print_r($result['OptedIn'], 1),
    )));
  }
  else {
    drupal_set_message(t('Not  found recipient'));
  }
}

/**
 * Form constructor for the update recipient form.
 *
 * @see silverpop_updaterecipient_form_validate()
 * @see silverpop_updaterecipient_form_submit()
 */
function silverpop_updaterecipient_form() {
  $form['oldmail'] = array(
    '#title' => t('Old email'),
    '#type' => 'textfield',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['newmail'] = array(
    '#title' => t('New email'),
    '#type' => 'textfield',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_updaterecipient_form().
 *
 * Check valid emails.
 *
 * @see silverpop_updaterecipient_form()
 * @see silverpop_updaterecipient_form_submit()
 */
function silverpop_updaterecipient_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['newmail'])) {
    form_set_error('newmail', t('This email is not valid!'));
  }
}

/**
 * Form submission handler for silverpop_updaterecipient_form().
 *
 * Update recipient.
 *
 * @see silverpop_updaterecipient_form()
 * @see silverpop_updaterecipient_form_validate()
 */
function silverpop_updaterecipient_form_submit($form, &$form_state) {
  if ($form_state['values']['oldmail'] && $form_state['values']['newmail']) {
    $silverpop = new Silverpop();
    $silverpop->updaterecipient(variable_get('silverpop_api_baseid', FALSE), $form_state['values']['oldmail'], $form_state['values']['newmail']);
    drupal_set_message(t('Done'));
  }
}

/**
 * Form constructor for the Silverpop create table.
 *
 * @see silverpop_addtable_form_submit()
 */
function silverpop_addtable_form() {
  $form['tablename'] = array(
    '#title' => t('Enter table name'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_addtable_form().
 *
 * @see silverpop_addtable_form()
 */
function silverpop_addtable_form_submit($form, &$form_state) {
  global $user;
  $silverpop = new Silverpop();
  $name = trim($form_state['values']['tablename']);
  $result = $silverpop->createtable($name, $name);
  if ($result) {
    drupal_set_message(t('Table id is: %result', array('%result' => print_r($result, 1))));
  }
  else {
    drupal_set_message(t('Error, try another name'));
  }
}

/**
 * Form constructor for the get list metadata form.
 *
 * @see silverpop_getlistmetadata_form_validate()
 * @see silverpop_getlistmetadata_form_submit()
 */
function silverpop_getlistmetadata_form() {
  $form = array();

  $form['listid'] = array(
    '#title' => t('Enter list id'),
    '#type' => 'textfield',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_getlistmetadata_form().
 *
 * Check if is numeric list ID.
 *
 * @see silverpop_getlistmetadata_form()
 * @see silverpop_getlistmetadata_form_submit()
 */
function silverpop_getlistmetadata_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['listid'])) {
    form_set_error('listid', t('This id is not valid!'));
  }
}

/**
 * Form submission handler for silverpop_getlistmetadata_form().
 *
 * Get list metadata.
 *
 * @see silverpop_getlistmetadata_form()
 * @see silverpop_getlistmetadata_form_validate()
 */
function silverpop_getlistmetadata_form_submit($form, &$form_state) {
  $silverpop = new Silverpop();
  $result = $silverpop->getlistmetadata($form_state['values']['listid']);
  drupal_set_message(t('debug: %result', array('%result' => print_r($result, 1))));
}

/**
 * Form constructor for the adding contact to contact list form.
 *
 * @see silverpop_addcontacttocontactlist_form_validate()
 * @see silverpop_addcontacttocontactlist_form_submit()
 */
function silverpop_addcontacttocontactlist_form() {
  $form['listid'] = array(
    '#title' => t('Enter contact list id'),
    '#type' => 'textfield',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['contactid'] = array(
    '#title' => t('Enter contact id'),
    '#type' => 'textfield',
    '#rows' => 10,
    '#required' => TRUE,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

/**
 * Form validation handler for silverpop_addcontacttocontactlist_form().
 *
 * Check if is numeric list ID and contact ID.
 *
 * @see silverpop_addcontacttocontactlist_form()
 * @see silverpop_addcontacttocontactlist_form_submit()
 */
function silverpop_addcontacttocontactlist_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['listid'])) {
    form_set_error('listid', t('This id is not valid!'));
  }
  if (!is_numeric($form_state['values']['contactid'])) {
    form_set_error('contactid', t('This id is not valid!'));
  }
}

/**
 * Form submission handler for silverpop_addcontacttocontactlist_form().
 *
 * @see silverpop_addcontacttocontactlist_form()
 * @see silverpop_addcontacttocontactlist_form_validate()
 */
function silverpop_addcontacttocontactlist_form_submit($form, &$form_state) {
  $silverpop = new Silverpop();
  $result = $silverpop->addcontacttocontactlist($form_state['values']['listid'], $form_state['values']['contactid']);
  drupal_set_message(t('debug: %result', array('%result' => print_r($result, 1))));
}

/**
 * Form constructor for the the get mailings for user form.
 */
function silverpop_getsentmailingsforuser_form() {
  $form['from'] = array(
    '#type' => 'textfield',
    '#title' => t('From date'),
    '#maxlength' => 30,
    '#size' => 30,
  );
  $form['to'] = array(
    '#title' => t('To date'),
    '#type' => 'textfield',
    '#maxlength' => 30,
    '#size' => 30,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('View'),
  );

  return $form;
}

/**
 * Form constructor for the mailing templates form.
 *
 * @see silverpop_getmailingstemplates_form_submit()
 */
function silverpop_getmailingstemplates_form() {
  $form['from'] = array(
    '#type' => 'date_popup',
    '#title' => t('Last modified start date'),
    '#date_format' => 'm/d/Y - H:i',
    '#date_year_range' => '-5:+1',
  );
  $form['to'] = array(
    '#type' => 'date_popup',
    '#title' => t('Last modified end date'),
    '#date_format' => 'm/d/Y - H:i',
    '#date_year_range' => '-4:+2',
  );
  $form['tos'] = array(
    '#title' => t('To date'),
    '#type' => 'textfield',
    '#maxlength' => 30,
    '#size' => 30,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('View'),
  );

  return $form;
}

/**
 * Form submission handler for silverpop_getmailingstemplates_form().
 *
 * @see silverpop_getmailingstemplates_form_submit()
 */
function silverpop_getmailingstemplates_form_submit($form, &$form_state) {
  $start = format_date(strtotime($form_state['values']['from']), 'custom', "m/d/Y  H:i:s");  /*format dates to send through silverpop API function*/
  $end = format_date(strtotime($form_state['values']['to']), 'custom', "m/d/Y  H:i:s");
  drupal_set_message(t('start: %start', array('%start' => print_r($start, 1))));
  drupal_set_message(t('end: %end', array('%end' => print_r($end, 1))));
  $silverpop = new Silverpop();
  $result = $silverpop->getmailingstemplates($start, $end);
}

/**
 * Form constructor for the create list form.
 *
 * @see silverpop_createlist_form_submit()
 */
function silverpop_createlist_form() {
  $form['baseid'] = array(
    '#type' => 'textfield',
    '#title' => t('Database id'),
    '#default_value' => variable_get('silverpop_api_baseid', FALSE),
    '#disabled' => TRUE,
    '#maxlength' => 30,
    '#size' => 30,
  );
  $form['listname'] = array(
    '#title' => t('List name'),
    '#type' => 'textfield',
    '#maxlength' => 30,
    '#size' => 30,
  );
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form submission handler for silverpop_createlist_form().
 *
 * Create list and write data to DB.
 *
 * @see silverpop_createlist_form()
 */
function silverpop_createlist_form_submit($form, &$form_state) {
  global $user;
  $baseid = $form_state['values']['baseid'];
  $listname  = $form_state['values']['listname'];
  $silverpop = new Silverpop();
  $list = $silverpop->create_contactlist(variable_get('silverpop_api_baseid', FALSE), $listname);
  if (is_numeric($list)) {
    $table = 'silverpop_contactlists';
    $record = new stdClass();
    $record->suser = $user->uid;
    $record->listname = $listname;
    $record->listid = $list;
    drupal_write_record($table, $record);
  }
  else {
    drupal_set_message(t('Error, try another name!'));
  }
}
